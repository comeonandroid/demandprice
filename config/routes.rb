  Rails.application.routes.draw do


  root 'main#index'
  ActiveAdmin.routes(self)

  get 'categories/import', to: 'categories#import'
  get 'categories/import_city', to: 'categories#import_city'
  get 'categories/:id', to: 'categories#index', as: 'categories'

  get 'contact', to: 'pages#contact', as: 'contact'
  get 'rules',   to: 'pages#rules',   as: 'rules'
  get 'about',   to: 'pages#about',   as: 'about'
  get 'dogovor', to: 'pages#dogovor', as: 'dogovor'
  get 'news',    to: 'pages#news',    as: 'news'

  # resources :users, only: [:show, :create]
  get 'profile(/:id)', to: 'users#index', as: 'profile'
  get 'users/login'
  get 'users/reg'
  get 'users/new_password'
  get 'profile/requests(/:type/(:id))', to: 'users#requests', as: 'profile_requests'
  get 'profile/bids(/:type/(:id))',       to: 'users#bids', as: 'profile_bids'
  get 'profile/managed(/:type(:id/))',     to: 'users#managed', as: 'profile_managed'

  patch 'requests/addMinimumPrice/:id', to: 'requests#add_minimum_price', as: 'addMinimumPrice'
  get   'requests/searchModal', to: 'requests#search_modal', as: 'search_modal'
  get   'requests/cat:id', to: 'requests#index', as: 'requests_category'
  get   'requests/archive', to: 'requests#archive', as: 'requests_archive'
  get   'requests/await', to: 'requests#await', as: 'requests_await'
  get   'requests/refused/:id', to: 'requests#refused', as: 'request_refused'
  get   'requests/payed/:id', to: 'requests#payed', as: 'request_payed'
  get   'requests/close/:id', to: 'requests#close', as: 'request_close'
  get   'requests/take/:id', to: 'requests#take', as: 'request_take'

  get  'invoice/:id', to: 'invoice#show', as: 'invoice'
  get  'bill/:id', to: 'invoice#bill', as: 'bill'

  resources :contacts,
    :controller => 'contacts',
    :only       => [:create]

  post '/documents/:temp_key', to: 'documents#create', as: 'documents_create'
  get '/documents/:id', to: 'documents#show', as: 'documents_show'
  delete '/documents/:id', to: 'documents#delete', as: 'documents_delete'
  resources :requests
  resources :bids, :only => [:create]

  devise_for :users, controllers: {sessions: 'sessions', registrations: 'registrations', passwords: 'passwords'}
  devise_for :admin_users, ActiveAdmin::Devise.config
  #get  'main#index', as: 'user_root'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with 'rake routes'.

  # You can have the root of your site routed with 'root'
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
