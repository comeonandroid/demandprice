$ ->
  $(document).on("ajax:success","form#new_user", (event, jqxhr, settings, exception) ->
    window.location.href = '/'
  ).on("ajax:error","form#new_user", (event, jqxhr, settings, exception) ->
    $(event.currentTarget).render_form_errors( $.parseJSON(jqxhr.responseText).errors)
  )


$ ->
  $(document).on("ajax:success","form#login_user", (event, jqxhr, settings, exception) ->
    window.location.href = '/'
  ).on("ajax:error","form#login_user", (event, jqxhr, settings, exception) ->
    $(event.currentTarget).render_one_form_errors($.parseJSON(jqxhr.responseText).error))


$ ->
  $(document).on("ajax:success","form#new_password_user", (event, jqxhr, settings, exception) ->
    $(this).siblings('h1').text("Ваш новый пароль был отправлен Вам на почту контактного лица указанную при регистрации")
    $(this).remove()
    setTimeout ()->
      $("[data-remodal-id=add-bid").remodal().close()
      $("[data-remodal-id=add-bid").remove()
    ,3000
  ).on("ajax:error","form#new_password_user", (event, jqxhr, settings, exception) ->
    console.log jqxhr
    $(event.currentTarget).render_form_errors( $.parseJSON(jqxhr.responseText).errors)
  )
