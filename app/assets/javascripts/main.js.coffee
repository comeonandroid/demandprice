bindMyStuff = ()->
  jQuery('input.datetimepicker').each ()->
    $(this).datetimepicker
      formatDate: 'y-m-d',
      format: 'Y-m-d H:i',
      timepicker: !$(this).hasClass('no-time'),
      allowBlank: true,
      allowTimes: true,
      defaultSelect: false,
      validateOnBlur: false,
      dayOfWeekStart: 1,
      lang: 'ru'

  $("form.dropzone").dropzone(
    {
      url: $("form.dropzone").attr('action')
      maxFilesize: 5
      maxFiles: 5
      addRemoveLinks: true
      dictRemoveFile: ""
      dictCancelUpload: ""
      acceptedFiles: ".jpeg,.jpg,.gif,.png,.doc,.docx,.xls, .xlsx,.txt,.pdf,.rtf,
                      .ppt, .pptx,.pps,.ppsx"
      dictDefaultMessage: "Кликните или перенесите файлы для загрузки"
      dictInvalidFileType: "Неверный тип файла"
      dictFileTooBig: "Максимальный размер файла 5МБ"
      dictMaxFilesExceeded: "Максимальное количество загружаеммых файлов 5"
      maxfilesexceeded: (file)->
        this.removeFile(this.files[5])
        $(".dropzone .help-block").text("Максимальное количество файлов 5")
      init: ()->
        dropzone = this
        if $('.saved_files').length > 0
          $('.saved_files').each ()->
            mockFile = { name: $(this).data('name'), id: $(this).data('id'), size: $(this).data('filesize'),mockFile: true }
            dropzone.emit("addedfile", mockFile)
            if $(this).data('filetype') == 'image'
              dropzone.emit("thumbnail", mockFile,$(this).data('src'))
            dropzone.emit("complete", mockFile)
            dropzone.files.push mockFile
          existingFileCount = $('.saved_files').length
          dropzone.options.maxFiles = dropzone.options.maxFiles - existingFileCount
        this.on 'removedfile', (file)->
          if file.xhr || file.id?
            id = if file.xhr?.response? then (JSON.parse(file.xhr.response).id) else file.id
            if file.mockFile? && file.mockFile is true then  this.options.maxFiles++
            $.ajax
              url:  "/documents/" + id,
              type: 'DELETE'
        this.on 'addedfile', (file)->
          i = 0
          $(".dropzone .help-block").text("")
          for key, val of this.files
            if val.status == 'error' && val.name != file.name then this.removeFile(this.files[key])
            if val.name == file.name
              if i > 0
                this.removeFile(this.files[this.files.length - 1])
                $(".dropzone .help-block").text("Файл с таким именем уже загружен")
              i++

    }
  )


  $ ->
    text = $("label[for='q_price_lteq']").text()
    $("label[for='q_price_lteq']").text("#{text} (#{parseFloat($("#price_lteq").val()).toFixed(1)})")
    $("#price_lteq").change ->
      $("label[for='q_price_lteq']").text("#{text} (#{parseFloat(this.value).toFixed(1)})")



  $('body').on 'click','.payment-block-item', (e)->
    $(this).find('input[type=radio]').click()
  $('body').on 'click','input[type=radio]', (e)->
    e.stopPropagation()

  $('body').on 'submit', 'form.request', (e)->
    $(this).find('.form-cover').addClass 'show'

$.fn.render_form_errors = (errors) ->
  @clear_previous_errors()
  model = @data('model')
  form = $(this)

  $.each errors, (field, messages) ->
    $input = form.find('[name="' + model + '[' + field + ']"]')
    $input.closest('li').addClass('has-error')
    $input.closest('li').prepend "<div class='help-block'>#{messages.join(', ').replace(/не может быть пустым,|не может быть пустым|имеет непредусмотренное значение/gi,'')}</div>"

$.fn.render_one_form_errors = (errors) ->
  @clear_previous_errors()
  this.prepend "<div class='help-block'>#{errors}</div>"

$.fn.clear_previous_errors = ->
  this.find('.has-error').removeClass 'has-error'
  this.find('.help-block').remove()

$(document).ready ()->
  bindMyStuff()
  $('body').on 'click','.modal-trigger',(e)->
    e.preventDefault()
    target = $(this).data('target')
    href   = $(this).attr('href')
    if $("[data-remodal-id=#{target}]").length > 0
      $("[data-remodal-id=#{target}]").remodal().open()
    else
      $.get href, (response)->
        $('body').append response
        $("[data-remodal-id=#{target}]").remodal().open()
        bindMyStuff()
