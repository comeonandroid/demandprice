# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $(document).on("ajax:success","form#new_contact_us_contact", (event, jqxhr, settings, exception) ->
    $("[data-remodal-id=contact-success").remodal().open()
    setTimeout ()->
      window.location.href = '/'
    , 4000  
  ).on("ajax:error","form#new_contact_us_contact", (event, jqxhr, settings, exception) ->
    $(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText)))
