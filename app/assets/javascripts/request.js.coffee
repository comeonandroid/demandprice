# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $(document).on("ajax:success","form.request", (event, jqxhr, settings, exception) ->
    window.location.href = jqxhr.location
    # $(this).siblings('h1').text(jqxhr.responseText)
    # $(this).remove()
    # setTimeout ()->
    #   $("[data-remodal-id=add-bid").remodal().close()
    #   $("[data-remodal-id=add-bid").remove()
    # ,3000
  ).on("ajax:error","form.request", (event, jqxhr, settings, exception) ->
    $(this).find('.form-cover').removeClass 'show'
    $(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText)))


  $(document).on("ajax:success","form#add_minimum_price", (event, jqxhr, settings, exception) ->
    window.location.href = jqxhr.location
  ).on("ajax:error","form#add_minimum_price", (event, jqxhr, settings, exception) ->
    $(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText))
  )


# ?




$(document).ready ()->

  $('body').on 'click','input[type=text]',(e)->
    modal_attr = $(this).data('load')
    $(".modal-list[data-target=#{modal_attr}").toggleClass 'visible'

  $('body').on 'click','.modal-list .close-btn',(e)->
    $(this).parent('.modal-list').toggleClass 'visible'

  $('body').on 'click','.category-item-heading',(e)->
    $('.exp-content').slideUp 'fast'
    if $(this).next('.exp-content').css('display') is 'block'
      $(this).next('.exp-content').slideUp 'fast'
    else
      $(this).next('.exp-content').slideDown 'fast'

  $('body').on 'click','.exp-content ul > li',(e)->
    $(this).children('ul').slideToggle 'fast'


  $('body').on 'click','.modal-list a',(e)->
    e.preventDefault()
    modal_attr = $(this).parents('.modal-list').data('target')
    text = $(this).text()
    val  = $(this).data('category-id')
    $("##{modal_attr}_id").val val
    $("##{modal_attr}").val text
    $(".modal-list").removeClass 'visible'

  $('body').on 'click', '.choose-cat a', (e)->
    e.preventDefault()
  $('body').on "change","input[name='request[payment_term]']", (e)->
    if $(this).val() is 'contract'
      $("#request_payment_desc").addClass 'show'
    else
      $("#request_payment_desc").removeClass 'show'
