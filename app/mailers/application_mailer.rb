class ApplicationMailer < ActionMailer::Base
  default from: "info@cenasprosa.ru"
  layout 'mailer'
end
