class MainMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.main_mailer.bill.subject
  #
  def bill(bid)
    @bid      = bid
    @request  = @bid.request
    @provider = @bid.user
    mail(to: [@request.user.email_contact,@request.manager.email_contact], subject: 'Счет с портала Цена Спроса')
  end

  def new_request(request)
    @request = request
    mail(to: [@request.user.email_contact,"info@cenasprosa.ru"], subject: 'Ваша заявка ожидает проверки')
  end

  def aprove_request(request)
    @request = request
    mail(to: @request.user.email_contact, subject: 'Ваша заявка проверена. Цена Спроса')
  end

  def decline_request(request)
    @request = request
    mail(to: @request.user.email_contact, subject: 'Ваша заявка отклонена. Цена Спроса')
  end

  def new_user(user)
    @user = user
    mail(to: @user.email_contact, subject: 'Благодарим за регистрацию, на портале Цена Спроса')
  end

  def aprove_user(user)
    @user = user
    mail(to: @user.email_contact, subject: 'Ваш аккаунт подтвержден')
  end
end
