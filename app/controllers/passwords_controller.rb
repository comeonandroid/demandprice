class PasswordsController < Devise::PasswordsController
  respond_to :json
  before_filter :check_remember_word, :only => :create
  # def sign_in_params
  #   devise_parameter_sanitizer.sanitize(:sign_in)
  #   params.require(:user).permit(:username, :password)
  # end

  def check_remember_word
    user = User.where(email_contact: params["user"]["email_contact"]).first

    if user.present?
      if user.remember_word != params["user"]["remember_word"]
        render json: {:errors => {:remember_word => ["Не правильное проверочное слово"]}}, status: :unprocessable_entity and return
      end
    end
  end
end
