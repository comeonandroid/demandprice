class PagesController < ApplicationController
  def contact
    @page = Page.where('alias = ?','contact').first
    render 'default'
  end

  def rules
    @page = Page.where('alias = ?','rules').first
    render 'default'
  end

  def about
    @page = Page.where('alias = ?','about').first
    render 'default'
  end

  def dogovor
    @page = Page.where('alias = ?','dogovor').first
    render 'default'
  end

  def news
    @page = Page.where('alias = ?','news').first
    render 'default'
  end
end
