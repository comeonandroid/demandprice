class UsersController < ApplicationController
  load_and_authorize_resource

  def index
    @user = get_user
  end

  def show
    puts 'TESt'
  end

  def requests
    @user = get_user
    if params[:type] && params[:type] == "archived"
      @requests = Request.archived.where({:user_id =>@user.id})
    else
      @requests = Request.active.where({:user_id =>@user.id})
    end
  end

  def bids
    @user = get_user
    if params[:type] && params[:type] == "archived"
      @bids     = Bid.unscoped.where(:user_id => @user.id).joins(:request)
      .where("status != ? OR closing_time < ?",Request::STATUS_TYPES[1],Time.current).select(:request_id).distinct
    else
      @bids     = Bid.unscoped.where(:user_id => @user.id).joins(:request)
      .where("status  = ? AND closing_time > ?",Request::STATUS_TYPES[1],Time.current).select(:request_id).distinct
    end
  end

  def managed
    @user = get_user
    if params[:type] && params[:type] == "archived"
      @requests = Request.archived.where({:manager_id =>@user.id})
    else
      @requests = Request.active.where({:manager_id =>@user.id})
    end
    render 'requests'
  end


  def search
    render 'modals/_search', :layout => false
  end

  def login
    render 'modals/_login', :layout => false
  end

  def reg
    render 'modals/_reg', :layout => false
  end

  def new_password
    render 'modals/_new_password', :layout => false
  end

  private
    def get_user
      if !params[:id]
        current_user
      else
        User.find(params[:id])
      end
    end
end
