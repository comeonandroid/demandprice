class BidsController < ApplicationController
  # TODO: Не забыть сделать проверки и валидацию в модели
  load_and_authorize_resource

  def create
    @bid = Bid.new
    @request = Request.includes(:bids).find(bid_params['request_id'])
    if !@request.id.nil? && user_signed_in? && (@request.bids.last.nil? || @request.bids.last.user_id != current_user.id)
      @bid.user_id    = current_user.id
      @bid.request_id = @request.id
      current_price   = @request.minimum_price.blank? ? @request.price.to_i : @request.minimum_price.to_i
      minimum_price   = current_price - (@request.price.to_i * 0.01)

      @bid.price             = minimum_price.ceil
      @request.minimum_price = minimum_price.ceil
      @bid.save && @request.save(:validate => false)
      respond_to do |format|
        if
          format.js { render @bid, status: 200}
        else
          format.js { render json: @bid.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  private
    def bid_params
      params.require(:bid).permit(:request_id,:price)
    end
end
