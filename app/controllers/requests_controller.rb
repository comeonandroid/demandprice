class RequestsController < ApplicationController
  before_action :authenticate_user!, :only => :create
  load_and_authorize_resource

  def index
    if params[:id]
      @q = Request.active.where(:category_id => params[:id]).page(params[:page]).per(30).ransack(params[:q])
    else
      @q = Request.active.page(params[:page]).per(30).ransack(params[:q])
    end
    @requests = @q.result(distinct: true)
  end

  def archive
    @q = Request.archived.page(params[:page]).per(30).ransack(params[:q])
    @requests = @q.result(distinct: true)
    render 'index'
  end

  def await
    @q = Request.await.page(params[:page]).per(30).ransack(params[:q])
    @requests = @q.result(distinct: true)
    render 'index'
  end

  def show
    @request = Request.includes([{:bids => :user}, :documents]).find_by_id(params[:id])
  end

  def new
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    @temp_key = (0...50).map { o[rand(o.length)] }.join
    render 'modals/_new_request', :layout => false
  end

  def search_modal
    render 'modals/_search', :layout => false
  end

  def edit
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    @temp_key = (0...50).map { o[rand(o.length)] }.join
    render 'modals/_new_request', :layout => false
  end

  def create
    @request = Request.new(request_params)
    @request.user_id = current_user.id
    @request.status  = 'disable'
    @request.minimum_price = request_params['price']
    respond_to do |format|
      if @request.save
        format.js { render json: {responseText: "Ваша заявка успешно добавлена на портал Цена Спроса и ожидает подтверждения менеджера.", location: request_path(@request)}, status: :created }
        MainMailer.new_request(@request).deliver_later
      else
        format.js { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @request = Request.find(params[:id])
    update_params = request_params
    update_params['minimum_price'] = update_params['price']
    respond_to do |format|
      if @request.update_attributes(update_params)
        format.js { render json: {responseText: "Заявка успешно отредактирована", location: request_path(@request)}, status: :created }
      else
        format.js { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @request = Request.find(params[:id])
    MainMailer.decline_request(@request).deliver_later
    @request.destroy
    respond_to do |format|
      format.html { redirect_to requests_path }
      format.json { head :no_content }
    end
  end

  def take
    @request = Request.find(params[:id])
    @request.update_attributes({:manager_id => current_user.id, :status => Request::STATUS_TYPES[1]})
    MainMailer.aprove_request(@request).deliver_later
    respond_to do |format|
      format.html { redirect_to request_path(@request) }
      format.json { head :no_content }
      format.js
    end
  end

  def refused
    @request = Request.find(params[:id])
    @request.update_attributes({:status => Request::STATUS_TYPES[3]})
    respond_to do |format|
      format.html { redirect_to request_path(@request) }
      format.json { head :no_content }
      format.js
    end
  end

  def payed
    @request = Request.find(params[:id])
    @request.update_attributes({:status => Request::STATUS_TYPES[4]})
    respond_to do |format|
      format.html { redirect_to request_path(@request) }
      format.json { head :no_content }
      format.js
    end
  end

  def close
    @request = Request.find(params[:id])
    @request.update_attributes({:status => Request::STATUS_TYPES[3]})
    respond_to do |format|
      format.html { redirect_to request_path(@request) }
      format.json { head :no_content }
      format.js
    end
  end

  def add_minimum_price
    @request = Request.find(params[:id])
    params   = add_minimum_price_params
    params['minimum_price'] = params['price']

    respond_to do |format|
      if @request.update_attributes(params) and !params['price'].blank?
        if current_user.role == 'provider'
          @bid = Bid.new
          @bid.user_id    = current_user.id
          @bid.request_id = @request.id
          @bid.price      = params['minimum_price']
          @bid.save
        end
        format.js { render json: {location: request_path(@request)}, status: :created }
      else
        format.js { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def request_params
      params.require(:request).permit(:name,:minimum_price, :section,:price,:closing_time,:delivery_time,:city,:description,:payment_term,:delivery_term,:nds, :category_id, :payment_desc, :file, :temp_key)
    end

    def add_minimum_price_params
      params.require(:request).permit(:price)
    end

end
