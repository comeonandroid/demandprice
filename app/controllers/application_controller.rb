class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  before_filter :correct_domain!

  private
  def correct_domain!
    if request.original_fullpath.length > 1 &&  request.original_fullpath[-1, 1] == "/"
      redirect_to request.original_fullpath.slice(0, request.original_fullpath.length - 1)
    end
    if request.host == 'www.cenasprosa.ru'
      redirect_to 'http://cenasprosa.ru', :status => 301  # or explicitly 'http://www.mysite.com/'
    end
  end
end
