class InvoiceController < ApplicationController
  authorize_resource :class => false

  def show
    @bid      = Bid.find(params[:id])
    @request  = @bid.request
    @provider = @bid.request.user
    @tax     =  (@request.price.to_i - @bid.price.to_i)/5.0
    #@tax      =  (100000 - 90000)/5.0
    @nds     = ((@tax*18) / 118.0).round(2)
    if @tax > 500
      @price = @tax.round(-2)
      numbers_a = @tax.ceil.to_s.split("")
      if ((numbers_a[numbers_a.length-2] + numbers_a[numbers_a.length-1]).to_i != 0) && (numbers_a[numbers_a.length-2] + numbers_a[numbers_a.length-1]).to_i <= 50
        @price += 100
      end
    else
      @price = 500
    end
  end

  def bill
    @bid     = Bid.find(params[:id])
    @request = @bid.request
    provider = @bid.user
    if @request.update_attributes({:status => 'check_sended'})
      MainMailer.bill(@bid).deliver_later
    end
    respond_to do |format|
      format.html { redirect_to request_path(@request) }
      format.json { head :no_content }
      format.js
    end
  end
end
