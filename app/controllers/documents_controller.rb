class DocumentsController < ApplicationController
  def show
    @document = Document.find(params[:id])
    render 'modals/_image', :layout => false
  end

  def create
    @file = Document.new(
      temp_key: params['temp_key'],
      file: params['file']
    )
    @file.save!
    render json: @file
  end

  def delete
    @doc = Document.find(params['id'])
    @doc.destroy
    render nothing: true
  end
end
