class CategoriesController < ApplicationController
  def index
    @category = Category.find(params[:id])
  end

  # def import
  #   Category.import("#{Rails.root}/public/categories.csv")
  #   redirect_to root_url, notice: "Products imported."
  # end

end
