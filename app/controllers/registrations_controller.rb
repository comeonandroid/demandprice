class RegistrationsController < Devise::RegistrationsController
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token

  def sign_up_params
    devise_parameter_sanitizer.sanitize(:sign_up)
    params.require(:user).permit(:email_company,:email_contact, :email, :password, :role, :company_name, :company_phone, :ur_adress, :fis_adress, :inn, :kpp, :contact_phone, :fio_contact, :remember_word, :terms, :username,:password_confirmation, :post_index)
  end
end
