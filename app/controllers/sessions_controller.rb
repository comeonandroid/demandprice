class SessionsController < Devise::SessionsController
  respond_to :html, :json


  def sign_in_params
    devise_parameter_sanitizer.sanitize(:sign_in)
    params.require(:user).permit(:username, :password)
  end

  
end
