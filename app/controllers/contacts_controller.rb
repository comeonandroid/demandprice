class ContactsController < ContactUs::ContactsController

  def create
    @contact = ContactUs::Contact.new(params[:contact_us_contact])
    respond_to do |format|
      if @contact.save
        format.js { render json: @contact.errors, status: 200}
      else
        format.js { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @contact = ContactUs::Contact.new
    render_new_page
  end

  protected

    def render_new_page
      case ContactUs.form_gem
      when 'formtastic'  then render 'new_formtastic1'
      when 'simple_form' then render 'new_simple_form'
      else
        render 'new'
      end
    end

end
