ActiveAdmin.register Category do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :name, :parent_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  form do |f|
    f.inputs "Категории" do
      f.input :name,            :label => "Имя"
      f.input :parent_id,   :label => "Родительская категория",:as => :select, :collection => nested_set_options(Category, @category) {|i| "#{'-' * i.level} #{i.name}" }
    end
    f.submit
  end

end
