ActiveAdmin.register City do

  index do
    column :name
    column :created_at
    actions
  end

  filter :name
  filter :created_at

  permit_params :name
  form do |f|
    f.inputs do
      f.input :name,  label: "Город"
    end
    f.submit
  end
end
