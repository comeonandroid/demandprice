ActiveAdmin.register Page do

  permit_params :alias, :text
  form do |f|
    f.inputs do
      f.input :alias      
      f.input :text, as: :wysihtml5, commands: 'all', blocks: 'all', height: 'huge' #, :as => :ckeditor
    end
    f.submit
  end

end
