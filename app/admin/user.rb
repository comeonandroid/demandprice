ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#

permit_params :email_company, :email_contact, :role, :company_name, :company_phone, :ur_adress, :fis_adress, :inn, :contact_phone, :fio_contact, :role, :status, :post_index
#
# or
#
# permit_params do
#   permitted  [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

form do |f|
  f.semantic_errors *f.object.errors.keys
  f.inputs do
    f.input :status,  label: "Статус", :as => :select, :collection => User::status_options
    f.input :role,    label: "Роль"  , :as => :select, :collection => User::roles_options
    f.input :company_name,  label: "Название организации"
    f.input :company_phone, label: "Контактный телефон организации"
    f.input :email_company, label: "Email организации"
    f.input :ur_adress,     label: "Юридический адрес организации"
    f.input :fis_adress,    label: "Фактический адрес организации"
    f.input :inn,           label: "ИНН организации"
    f.input :kpp,           label: "КПП организации"
    f.input :post_index,    label: "Почтовый индекс организации"
    f.input :fio_contact,   label: "ФИО контактного лица"
    f.input :contact_phone, label: "Телефон контактного лица"
    f.input :email_contact, label: "Email контактного лица"
  end
  f.submit
end

end
