class Request < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  belongs_to :manager,:foreign_key => "manager_id", :class_name => "User"
  has_many :bids, dependent: :destroy
  has_many :documents, dependent: :destroy

  mount_uploader :file, DefaultUploader
  after_save :bind_documents

  validate :price_to_100

  default_scope { order(created_at: :desc) }
  scope :archived, -> {where("status  = ? OR status  = ?",STATUS_TYPES[3],STATUS_TYPES[4])}
  scope :active,   -> {where("status  = ?",STATUS_TYPES[1])}
  scope :await,    -> {where("status  = ? OR status  = ?",STATUS_TYPES[0],STATUS_TYPES[2])}


  PAYMENT_LABELS = [ "100%", "50%/50%", "Договорная"]
  PAYMENT_TYPES  = [ "100", "50", "contract"]

  STATUS_TYPES   = [ "disable", "active", "check_sended","refused", "payed"]
  STATUS_LABELS  = [ "Неактивная", "Активная", "Счет отправлен", "Отказ от оплаты", "Оплачено"]


  validates :name,:closing_time,:delivery_time,:section,:city,:payment_term, :presence => true

  validates :price, :numericality => {:only_integer => false, :allow_blank => true, :greater_than_or_equal_to => 10000, :message => "Минимальная сумма заявки не может быть меньше 10000р"}
  validates :price, :numericality => {:only_integer => false, :allow_blank => true, :less_than_or_equal_to => 1000000000, :message => "Максимальная сумма заявки не может быть больше 1000000000р"}

  validates_inclusion_of :delivery_term,:nds, in: [true, false]
  validates :payment_term, :inclusion => PAYMENT_TYPES
  validates :closing_time, date: { after: Proc.new  { Time.now + 8.hour }, message: 'Выберите более позднюю дату', allow_blank: true }, :on => :create
  validates :delivery_time, date: { after: Proc.new { Time.now + 3.hour }, message: 'Выберите более позднюю дату', allow_blank: true}, :on => :create

  ## Методы для человекапонятного вывода true:false соответствующих полей
  def get_nds
    self.nds ? "С НДС" : "Без НДС"
  end

  def get_delivery_term
    self.delivery_term ? "С доставкой" : "Без доставки"
  end

  def get_payment_term
    i = PAYMENT_TYPES.index(self.payment_term)
    PAYMENT_LABELS[i]
  end

  def get_status
    i = STATUS_TYPES.index(self.status)
    STATUS_LABELS[i]
  end

  ## Лейбл/значение для селектов методов оплаты
  def self.payment_term_options
    return [
        [Request::PAYMENT_LABELS[0],Request::PAYMENT_TYPES[0]],
        [Request::PAYMENT_LABELS[1],Request::PAYMENT_TYPES[1]],
        [Request::PAYMENT_LABELS[2],Request::PAYMENT_TYPES[2]]
    ]
  end

  def self.status_options
    return [
        [Request::STATUS_LABELS[0],Request::STATUS_TYPES[0]],
        [Request::STATUS_LABELS[1],Request::STATUS_TYPES[1]],
        [Request::STATUS_LABELS[2],Request::STATUS_TYPES[2]],
        [Request::STATUS_LABELS[3],Request::STATUS_TYPES[3]],
        [Request::STATUS_LABELS[4],Request::STATUS_TYPES[4]]
    ]
  end

  def price_to_100
    if !self.price.to_i.blank? and self.price.to_i % 100 > 0
      errors.add(:price, "Минимальный шаг цены 100 руб.")
    end
  end

  def bind_documents
    documents = Document.where(temp_key: self.temp_key).all
    documents.each do |document|
      document.request_id = self.id
      document.save
    end
  end
end
