class Bid < ActiveRecord::Base
  belongs_to :user
  belongs_to :request

  default_scope { order(created_at: :asc) }
end
