class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    alias_action :requests,:bids,:managed, :to => :user_info

    if user.role?
      can :manage, :all
    else
      can :read, :all
      can [:archive,:search_modal], Request
      can [:login, :reg, :new_password], User

    end

    cannot :update, [Request, User]
    cannot [:destroy,:take,:refused,:payed,:close], Request
    cannot :see_details, Request
    cannot :user_info, User
    cannot :bill, :invoice

    if user.role == 'costumer'
      cannot :bids, User
      cannot :create, Bid
      can :requests, User, :id => user.id
    elsif user.role == 'provider'
      cannot :requests, User
      cannot :create, Request
      can :bids, User, :id => user.id
    elsif user.role == 'manager'
      cannot :create, Request
      can [:see_details], Request
      can [:take, :update, :destroy], Request, :manager_id => nil, :status => Request::STATUS_TYPES[0]
      can [:refused,:payed], Request,:manager_id => user.id, :status => Request::STATUS_TYPES[2]
      can [:close], Request,:manager_id => user.id, :status => Request::STATUS_TYPES[1]
      can :user_info, User
      can :bill, :invoice
    end


    if user.role != 'manager'
      cannot [:read,:managed], User
      cannot :update, [Request, User]
      cannot [:destroy,:take], Request
      can :read, User, :id => user.id
    end
  end
end
