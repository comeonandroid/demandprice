class User < ActiveRecord::Base
  before_save :default_values
  has_many :bid
  has_many :requests, :dependent => :destroy
  after_create :send_email
  after_update :send_aprove_email

  REGISTRATION_ROLES_LABELS = ['Вы поставщик','Вы заказчик']
  REGISTRATION_ROLES =        ['provider','costumer']

  ROLES =        ['Поставщик','Заказчик','Менеджер']
  ROLES_LABELS = ['provider','costumer','manager']

  STATUS_TYPES   = [ "disable", "active"]
  STATUS_LABELS  = [ "Неактивен", "Активен"]


  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:username]

  attr_accessor :password_confirmation, :terms

  validates :role, :company_name, :company_phone, :ur_adress, :fis_adress, :contact_phone, :fio_contact, :remember_word,:inn, :kpp, :post_index, :presence => true

  validates :username, presence: true, uniqueness: { case_sensitive: false }
  validates :role,   :inclusion => REGISTRATION_ROLES, :on => :create
  validates :role,   :inclusion => ROLES_LABELS, :on => :update

  validates_inclusion_of :terms,  :in => ["1"], :message => "Вы должны согласится с условиями использования", :on => :create

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email_company, :email_contact, presence: true, format: { with: VALID_EMAIL_REGEX }

  validates :inn, :kpp, :post_index, :numericality => {:only_integer => true}, :allow_blank => true
  validates :password,:password_confirmation, :presence => true, :on => :create

  def default_values
    self.status ||= User::STATUS_TYPES[0]
    # for device password recovery
    self.email  = self.email_contact
  end

  def send_aprove_email
    if (self.status_changed? && self.status == "active")
      MainMailer.aprove_user(self).deliver_later
    end
  end
  def send_email
    MainMailer.new_user(self).deliver_later
  end

  def get_status
    i = STATUS_TYPES.index(self.status)
    STATUS_LABELS[i]
  end

  def self.roles_options
    return [
        [User::ROLES[0],User::ROLES_LABELS[0]],
        [User::ROLES[1],User::ROLES_LABELS[1]],
        [User::ROLES[2],User::ROLES_LABELS[2]]
    ]
  end

  def self.status_options
    return [
        [User::STATUS_LABELS[0],User::STATUS_TYPES[0]],
        [User::STATUS_LABELS[1],User::STATUS_TYPES[1]]
    ]
  end


  def email_required?
    false
  end

  def email_changed?
    false
  end
end
