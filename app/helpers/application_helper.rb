module ApplicationHelper
  @@providers_ids = Hash[]
  @@key = 1

  def provider_hide_id(id)
    if !@@providers_ids.key?(id)
      @@providers_ids[id] = @@key
      @@key = @@key + 1
    end
    @@providers_ids[id]
  end

  def reset_id
    @@providers_ids = Hash[]
    @@key = 1
  end

  def get_extension(filename)
    arr = filename.split('.').pop()
  end
end
