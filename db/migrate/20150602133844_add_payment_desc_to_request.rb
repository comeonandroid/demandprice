class AddPaymentDescToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :payment_desc, :string
  end
end
