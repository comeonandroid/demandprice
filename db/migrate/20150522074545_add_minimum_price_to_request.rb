class AddMinimumPriceToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :minimum_price, :integer
  end
end
