class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email_company
      t.string :email_contact
      t.string :password
      t.string :role
      t.string :company_name
      t.string :company_phone
      t.string :ur_adress
      t.string :fis_adress
      t.integer :inn
      t.string :contact_phone
      t.string :fio_contact

      t.timestamps null: false
    end
  end
end
