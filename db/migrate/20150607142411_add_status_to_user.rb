class AddStatusToUser < ActiveRecord::Migration
  def change
    add_column :users, :status, :string, null: false, default: "disable"
  end
end
