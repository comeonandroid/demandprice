class ChangeUsersTable < ActiveRecord::Migration
  def change
    remove_column :users, :email
    change_column :users, :inn,  :string
    change_column :users, :kpp,  :string
  end
end
