class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :file
      t.string :temp_key

      t.timestamps null: false
    end
  end
end
