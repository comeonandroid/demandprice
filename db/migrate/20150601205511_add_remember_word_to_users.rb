class AddRememberWordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :remember_word, :string
  end
end
