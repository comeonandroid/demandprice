class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :user_id
      t.string :name
      t.string :section
      t.datetime :closing_time
      t.string :delivery_time
      t.string :city
      t.text :description
      t.string :price
      t.string :payment_term
      t.boolean :delivery_term
      t.boolean :nds

      t.timestamps null: false
    end
  end
end
