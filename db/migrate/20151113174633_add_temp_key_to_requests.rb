class AddTempKeyToRequests < ActiveRecord::Migration
  def change
    add_column :requests, :temp_key, :string
  end
end
